# GUI builder for simulation

import kivy
kivy.require('1.9.1')

from kivy.config import Config
Config.set('graphics', 'borderless', '1')
Config.set('graphics', 'resizable', '0')
Config.set('graphics', 'position', 'custom')
Config.set('graphics', 'top', '20')
Config.set('graphics', 'left', '50')

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.button import Button
from kivy.uix.slider import Slider
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.colorpicker import ColorWheel
from kivy.uix.listview import ListView, ListItemButton
from kivy.adapters.listadapter import ListAdapter
from kivy.graphics import Rectangle, Ellipse, Color, Line
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.properties import NumericProperty

from functools import partial
from math import *
import re
import time
import tkinter

import sim_math

import copy

#Slider circle size adjustment
s=22 #default 32
Builder.load_string("""
<-Slider>:
    canvas:
        Color:
            rgb: 1, 1, 1
        BorderImage:
            border: (0, 18, 0, 18) if self.orientation == 'horizontal' else (18, 0, 18, 0)
            pos: (self.x + self.padding, self.center_y - sp(18)) if self.orientation == 'horizontal' else (self.center_x - 18, self.y + self.padding)
            size: (self.width - self.padding * 2, sp(36)) if self.orientation == 'horizontal' else (sp(36), self.height - self.padding * 2)
            source: 'atlas://data/images/defaulttheme/slider{}_background{}'.format(self.orientation[0], '_disabled' if self.disabled else '')
        Rectangle:
            pos: (self.value_pos[0] - sp("""+str(s/2)+"), self.center_y - sp("+str(s/2+1)+""")) if self.orientation == 'horizontal' else (self.center_x - (8), self.value_pos[1] - sp(8))
            size: (sp("""+str(s)+"), sp("+str(s)+"""))
            source: 'atlas://data/images/defaulttheme/slider_cursor{}'.format('_disabled' if self.disabled else '')
""")


class SliderCtrl(Slider):
    def __init__(self, step=1, value=1, min=1, max=100, **kwargs):
        super().__init__(**kwargs)
        self.size_hint=(0.7, None)
        self.size=(0, 20)
        self.step=step
        self.value=value
        self.min=min
        self.max=max


class ButtonCtrl(Button):
    def __init__(self, pos_hint={'center_x':0.5}, size_hint=(None, None), size=(50, 20), **kwargs):
        super().__init__(**kwargs)
        self.pos_hint=pos_hint
        self.size_hint=size_hint
        self.size=size


class LabelCtrl(Label):
    def __init__(self, font_size=12, size=(80, 20), pos_hint={'center_x':0.5}, **kwargs):
        super().__init__(**kwargs)
        self.font_size=font_size
        self.pos_hint=pos_hint
        self.size_hint=(None, None)
        self.size=size


class TextInputCtrl(TextInput):
    def __init__(self, font_size=12, pos_hint={'x':0.05}, **kwargs):
        super().__init__(**kwargs)
        self.multiline=False
        self.size_hint=(None, None)
        self.size=(40, 22)
        self.font_size=font_size
        self.pos_hint=pos_hint
        self.padding=(6, 3, 1, 2)

    pat = re.compile('[^0-9]')
    def insert_text(self, substring, from_undo=False):
        pat = self.pat
        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])
        return super().insert_text(s, from_undo=from_undo)

class ColorWheelCtrl(ColorWheel):
    def __init__(self, **kwargs):
        # Following two code lines are two fix bug where self.sv_s is called
        # sooner than assigned. Issue somewhere in kivy source.
        pdv = 20
        self.sv_s = [(float(x) / pdv, 1) for x in range(pdv)] + [
            (1, float(y) / pdv) for y in reversed(range(pdv))]
        super().__init__(**kwargs)
        self._radius = min(self.size) * 1
        self._origin = (self.center_x, self.center_y)
        self.init_wheel(dt = 0)
        self.sv_idx += 5
        self.recolor_wheel()

    # Fires when a color on color wheel is selected.
    def on__hsv(self, instance, value):
        super().on__hsv(instance, value)
        for c in self.parent.children:
            c.disabled = False
        self.parent.ids['color'].bcolor = self.rgba
        self.parent.ids['color'].canvas.before.clear()
        with self.parent.ids['color'].canvas.before:
            Color(*self.parent.ids['color'].bcolor)
            Rectangle(pos=((self.parent.size[0]-self.parent.ids['color'].size[0])//2, self.parent.ids['color'].pos[1]),
                            pos_hint=self.parent.ids['color'].pos_hint,
                            size=self.parent.ids['color'].size)
        self.parent.remove_widget(self)


class ListViewCtrl(ListView):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class SimulationWindow(Widget):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def update(self, dt):
        global event_queue, particle_list, particle_box
        global time_current, time_elapsed
        global pause, stop
        if stop:
            return
        # Time advances.
        if self.parent.ids['ctrlpane'].ids['time'].text == '':
            time_scale = 0.1
        else:
            time_scale = float(self.parent.ids['ctrlpane'].ids['time'].text)
        time_real = time.clock()
        time_diference = (time_real-time_elapsed) * (time_scale-1)
        time_elapsed = time_elapsed + (time_real-time_elapsed)*time_scale
        if not (pause or time_scale == 0):
            # Handles all events in given time frame (dt=elapsed-current).
            time_current, event_queue = sim_math.event_handling(
                        event_queue, particle_list,
                        particle_box, time_elapsed,
                        time_current)
            # Clears screen and draws new particle positions.
            self.draw_particles(particle_list)
        # Corrects simulation/real time mismatch due to time scaling.
        if not (time_scale == 1) or pause:
            time_frame = time_real - time_current
            time_elapsed = time_real
            time_current = time_real
            for e in event_queue:
                e[0] += time_frame
        return

    def draw_particles(self, particle_list, mode='refresh'):
        # For adding drawing instructions change mode to anything else.
        if mode == 'refresh':
            self.canvas.clear()
        for p in particle_list:
            with self.canvas:
                Color(*p.color)
                Ellipse(pos=(p.px-p.radius, p.py-p.radius), size=(p.radius*2, p.radius*2))

    def draw_highlights(self, particle_list):
        for p in particle_list:
            with self.canvas:
                Color(1.0, 1.0, 1.0)
                Line(circle=(p.px, p.py, p.radius+1))

    def place_particles(self, obj, touch):
        global particle_groups
        if self.collide_point(*touch.pos):
            particle_group = self.parent.ids['ctrlpane'].generate_particles(touch.pos)
            # Add new particles to canvas.
            self.draw_particles(particle_group, mode='add')
            names = []
            for g in particle_groups:
                names.append(g[0])
            n = 1
            name = "Group " + str(n)
            while name in names:
                n+=1
                name = "Group " + str(n)
            particle_groups.append([name, particle_group])
            self.parent.ids['ctrlpane'].update_list(particle_groups)


class ControlPanel(RelativeLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.size = 200, 500

    def generate_particles(self, pos=None):
        global particle_box, b_width, b_height
        global time_current, time_elapsed
        # Bounding box for particles
        amount = int(self.ids['count'].text)
        radius = int(self.ids['radius'].text)
        speed = int(self.ids['speed'].text)
        mass = int(self.ids['mass'].text)
        color = self.ids['color'].bcolor
        if pos == None:
            pos = (box_width/2, box_height/2)
        particle_group = sim_math.generate_particle_group(
                amount=amount, radius=radius,
                speed=speed, mass=mass, color=color,
                sx=pos[0], sy=pos[1]
                )
        sim_math.clean_particles(particle_group, b_width, b_height)
        return particle_group

    def start_simulation(self, btn):
        global event_queue, particle_list, particle_box, particle_groups
        global time_current, time_elapsed
        global stop
        global f
        f = open("log.txt", "w")
        # Generate particle list from particle groups.
        if not particle_groups:
            stop = True
            self.stop_simulation(None)
            return
        particle_list = []
        for g in particle_groups:
            for p in g[1]:
                particle_list.append(p)
        # if particle_list:
        if self.ids['time'].text == '':
            time_scale = 0.1
        else:
            time_scale = float(self.ids['time'].text)
        time_elapsed = time.clock()
        time_current = time_elapsed
        event_queue = sim_math.generate_event_qeueu(particle_list, particle_box, time_current)
        stop = False

    def pause_toggle(self, btn):
        global pause
        if pause:
            pause = False
            btn.text = "Pause"
        else:
            pause = True
            btn.text = "Resume"

    def stop_simulation(self, btn):
        global stop
        global f
        stop = True
        self.parent.ids['simwin'].canvas.clear()
        f.close()

    def choose_color(self, lbl, touch):
        if lbl.collide_point(*touch.pos) and not lbl.disabled:
            for c in self.children:
                c.disabled = True
            cw = ColorWheelCtrl(pos=lbl.pos, size_hint=(None, None))
            lbl.parent.add_widget(cw)

    def update_list(self, particle_groups):
        l_adapter = self.ids['list'].adapter
        selection = l_adapter.selection
        l_adapter.data = [str(g[0]) for g in particle_groups]
        if selection and l_adapter.data:
            if selection[0].index < len(l_adapter.data):
                item = l_adapter.get_view(selection[0].index)
            else:
                item = l_adapter.get_view(len(l_adapter.data) - 1)
            l_adapter.handle_selection(item)
            self.highlight_group(l_adapter)


    def remove_group(self, btn):
        # Same order of objects in particle_groups and list view items is assumed.
        global particle_groups
        l_adapter = btn.parent.ids['list'].adapter
        if l_adapter.selection:
            i = l_adapter.selection[0].index
            particle_groups.remove(particle_groups[i])
            particles = []
            for g in particle_groups:
                for p in g[1]:
                    particles.append(p)
            #btn.parent.parent.ids['simwin'].draw_particles(particles)
            self.update_list(particle_groups)

    def clear_list(self, btn):
        global particle_groups
        particle_groups = []
        self.update_list(particle_groups)
        self.parent.ids['simwin'].canvas.clear()

    def highlight_group(self, l_adapter):
        global particle_groups
        particles = []
        for g in particle_groups:
            for p in g[1]:
                particles.append(p)
        self.parent.ids['simwin'].draw_particles(particles)
        if l_adapter.selection:
            name = l_adapter.selection[0].text
            for g in particle_groups:
                if g[0] == name:
                    self.parent.ids['simwin'].draw_highlights(g[1])

def get_resolution():
    root = tkinter.Tk()
    width = root.winfo_screenwidth()
    height = root.winfo_screenheight()
    root.destroy()
    return width, height


class GUIApp(App):
    def build(self):
        win_width, win_height = get_resolution()
        Window.size = win_width-100, win_height-100
        #Window.size = 900, 500     
        root_widget = Widget()
        simwin = SimulationWindow(size=(win_width-300, win_height-100),
                                  pos=(0, 0),
                                  id='simwin'
                                  )
        root_widget.ids[simwin.id] = simwin
        ctrlpane = ControlPanel(pos=((simwin.pos[0] + simwin.size[0]), 0),
                                size=(200, win_height-100),
                                id='ctrlpane'
                                )
        root_widget.ids[ctrlpane.id] = ctrlpane
        spacing=45
        spacing_top=30
        spacing_b=spacing_top-8
        settings_labels = []
        lbl_settings = LabelCtrl(text='Settings',
                             font_size=18,
                             pos=(0, ctrlpane.size[1]-spacing_top),
                             )
        settings_labels.append(lbl_settings)
        lbl_list = LabelCtrl(text='Particle groups',
                             font_size=15,
                             pos_hint={'center_x':0.5, 'y':0.48}
                             )
        settings_labels.append(lbl_list)
        lbl_count = LabelCtrl(text='Particle count',
                          pos=(0, ctrlpane.size[1]-spacing*1)
                          )
        settings_labels.append(lbl_count)
        lbl_speed = LabelCtrl(text='Particle speed',
                          pos=(0, ctrlpane.size[1]-spacing*2)
                          )
        settings_labels.append(lbl_speed)
        lbl_radius = LabelCtrl(text='Particle size',
                           pos=(0, ctrlpane.size[1]-spacing*3)
                           )
        settings_labels.append(lbl_radius)
        lbl_mass = LabelCtrl(text='Particle mass',
                          pos=(0, ctrlpane.size[1]-spacing*4)
                          )
        settings_labels.append(lbl_mass)
        lbl_color = LabelCtrl(text='Choose particle color',
                          pos=(0, ctrlpane.size[1]-spacing*5-5),
                          size=(120, 20),
                          id='color'
                          )
        lbl_color.bcolor = (0.2, 0.7, 0.2, 1)
        settings_labels.append(lbl_color)
        lbl_time = LabelCtrl(text='Time scale',
                         pos=(0, ctrlpane.size[1]-spacing*9.7)
                         )
        settings_labels.append(lbl_time)

        settings_buttons = []
        btn_start = ButtonCtrl(text='Start',
                           pos=(0, 10),
                           pos_hint={'center_x':0.2}
                           )
        settings_buttons.append(btn_start)
        btn_pause = ButtonCtrl(text='Pause',
                           pos=(0, 10),
                           pos_hint={'center_x':0.5}
                           )
        settings_buttons.append(btn_pause)
        btn_stop = ButtonCtrl(text='Stop',
                          pos=(0, 10),
                          pos_hint={'center_x':0.8}
                          )
        settings_buttons.append(btn_stop)
        btn_remove = ButtonCtrl(text='Remove',
                          pos_hint={'center_x':0.3, 'y':0.2},
                          size=(70, 20)
                          )
        settings_buttons.append(btn_remove)
        btn_clear = ButtonCtrl(text='Clear',
                          pos=(0, 10),
                          pos_hint={'center_x':0.65, 'y':0.2}
                          )
        settings_buttons.append(btn_clear)

        settings_sliders = []
        spacing_left = 55
        spacing_b += 0
        sld_count = SliderCtrl(pos=(spacing_left, ctrlpane.height-spacing_b-spacing*1))
        settings_sliders.append(sld_count)
        sld_speed = SliderCtrl(pos=(spacing_left, ctrlpane.height-spacing_b-spacing*2),
                               max=500)
        settings_sliders.append(sld_speed)
        sld_radius = SliderCtrl(pos=(spacing_left, ctrlpane.height-spacing_b-spacing*3))
        settings_sliders.append(sld_radius)
        sld_mass = SliderCtrl(pos=(spacing_left, ctrlpane.height-spacing_b-spacing*4))
        settings_sliders.append(sld_mass)
        sld_time = SliderCtrl(pos=(spacing_left, ctrlpane.height-spacing_b-spacing*9.7),
                              min=0.1,
                              max=3.0,
                              value=1.0,
                              step=0.1)
        settings_sliders.append(sld_time)

        settings_txtin = []
        spacing_b += 2
        txt_count = TextInputCtrl(text=str(sld_count.value),
                                  pos=(5, ctrlpane.height-spacing_b-spacing*1),
                                  id='count'
                                  )
        settings_txtin.append(txt_count)
        txt_speed = TextInputCtrl(text=str(sld_speed.value),
                                  pos=(5, ctrlpane.height-spacing_b-spacing*2),
                                  id='speed'
                                  )
        settings_txtin.append(txt_speed)
        txt_radius = TextInputCtrl(text=str(sld_radius.value),
                                   pos=(5, ctrlpane.height-spacing_b-spacing*3),
                                   id='radius'
                                   )
        settings_txtin.append(txt_radius)
        txt_mass = TextInputCtrl(text=str(sld_mass.value),
                                   pos=(5, ctrlpane.height-spacing_b-spacing*4),
                                   id='mass'
                                   )
        settings_txtin.append(txt_mass)
        txt_time = TextInputCtrl(text=str(sld_time.value),
                                 pos=(5, ctrlpane.height-spacing_b-spacing*9.7),
                                 id='time'
                                 )
        settings_txtin.append(txt_time)
        for i in settings_txtin:
            ctrlpane.ids[i.id] = i
        ctrlpane.ids[lbl_color.id] = lbl_color


        root_widget.add_widget(ctrlpane)
        root_widget.add_widget(simwin)
        for w in settings_labels:
            ctrlpane.add_widget(w)
        for w in settings_txtin:
            ctrlpane.add_widget(w)
        for w in settings_buttons:
            ctrlpane.add_widget(w)
        for w in settings_sliders:
            ctrlpane.add_widget(w)

        with lbl_color.canvas.before:
            Color(*lbl_color.bcolor)
            Rectangle(pos=((lbl_color.parent.size[0]-lbl_color.size[0])//2, lbl_color.pos[1]),
                            pos_hint=lbl_color.pos_hint, size=lbl_color.size)

        ladapter = ListAdapter(cls=ListItemButton,
                               allow_empty_selection=True,
                               data=[str(i[0]) for i in particle_groups])
        lview = ListView(size_hint=(0.8, 0.2),
                         pos_hint={'x':0.1, 'y':0.27},
                         adapter=ladapter,
                         id='list'
                         )
        ctrlpane.add_widget(lview)
        ctrlpane.ids[lview.id] = lview
        # Event bindings for GUI objects
        for i, j in zip(settings_sliders, settings_txtin):
            i.bind(value=partial(self.update_txtin, j))
            j.bind(text=partial(self.update_sld, i))
        btn_pause.bind(on_press=ctrlpane.pause_toggle)
        btn_start.bind(on_press=ctrlpane.start_simulation)
        btn_stop.bind(on_press=ctrlpane.stop_simulation)
        btn_remove.bind(on_press=ctrlpane.remove_group)
        btn_clear.bind(on_press=ctrlpane.clear_list)
        lbl_color.bind(on_touch_down=ctrlpane.choose_color)
        ladapter.bind(on_selection_change=ctrlpane.highlight_group)
        simwin.bind(on_touch_up=simwin.place_particles)

        Clock.schedule_interval(simwin.update, 1.0/60.0)
        return root_widget


    def update_txtin(self, txtin_obj, sld_obj, value):
        value = int(value*10)/10
        if int(value) == value:
            txtin_obj.text = str(int(value))
        else:
            txtin_obj.text = str(value)

    def update_sld(self, sld_obj, txtin_obj, text):
        if text == '':
            return
        elif sld_obj.step >= 1 or '.' not in text:
            txtin_obj.txt = text.replace('.', '')
            value = int(text)
        else:
            value = float(text)
        if value > sld_obj.max:
            if type(value) is int:
                value = int(sld_obj.max)
            else:
                value = sld_obj.max
        elif value < sld_obj.min:
            value = sld_obj.min
        sld_obj.value = value
        txtin_obj.text = str(value)



if __name__ == '__main__':
    global time_elapsed, time_current
    time_elapsed = 0
    time_current = 0
    global event_queue, particle_list, particle_groups
    event_queue = []
    particle_list = []
    particle_groups = []
    win_width, win_height = get_resolution()
    b_width, b_height = win_width-300, win_height-100
    # particle box denoted by walls [x, y, line direction]
    global particle_box
    particle_box = []
    for a, b, c in [[0, 0, 0], [b_width, 0, pi/2], [0, 0, pi/2], [0, b_height, 0]]:
        w = sim_math.gen_wall(a, b, c)
        particle_box.append(w)
    global pause, stop, f
    pause = False
    stop = True
    f = None

    GUIApp().run()