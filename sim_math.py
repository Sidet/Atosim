#Core simulation code
import sys
import math
import random
import time
import itertools

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *


class Particle:
    """Wraper for all data and functions concerning a particle and it's interactions: speed, postion, radius, collisions, ..."""
    _id = itertools.count(1)

    # creation of particle; direction in radians
    def __init__(self, radius, position_x, position_y, speed, direction, mass=1, color=(0.2, 1, 0.2, 1)):
        self.radius = radius
        self.px = position_x
        self.py = position_y
        self.speed = speed
        self.direction = direction
        self.mass = mass
        self.polar_to_cartesian()
        self.color = color
        self.id = next(self._id)

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, d):
        self._direction = d % (math.pi*2)

    # Velocity is expressed in polar coordinates. Standard translation uses cartesian expression.
    def polar_to_cartesian(self):
        self.sx = self.speed * math.cos(self.direction)
        self.sy = self.speed * math.sin(self.direction)

    # When cartesian coordinates part of particle data is changed run this to also change polar coordinates part.
    def cartesian_to_polar(self): #velocity representation coordinates
        self.speed = math.hypot(self.sx, self.sy)
        self.direction = math.atan2(self.sy, self.sx)

    # Method for calculating final particle speed and direction,
    # when velocity change is expressed in polar coordinates.
    # Or simply vector addition.
    def change_polar_velocity(self, speedchange, changedirection):
        r1 = self.speed
        r2 = speedchange
        f1 = self.direction
        f2 = changedirection
        c = math.cos(f2 - f1)
        r = math.sqrt(r1**2 + r2**2 + 2*r1*r2*c)
        if r:
            f = f1 + math.acos((r1 + r2*math.cos(f2-f1)) / r)
        else:
            f = 0
        self.speed = r
        self.direction = f
        self.polar_to_cartesian()

    # Updates particle velocity after collision with
    # a wall of specified direction ir radians [0;pi)
    def collide_wall(self, wall_direction):
        self.direction = (wall_direction % math.pi)*2 - self.direction
        self.polar_to_cartesian()

    # When two particles meet calculates and updates velocities to new
    # values for participating particles.
    def collide_particles(self, p2):
        p1 = self
        # Collision direction.
        dc = math.atan2(p2.py - p1.py, p2.px - p1.px)
        d1 = p1.direction
        d2 = p2.direction
        # Velocity direction and collision direction difference.
        theta1 = dc-d1
        theta2 = dc-d2
        #Perpendicular direction to collision direction.
        d1p = dc - math.copysign(math.pi/2, math.sin(theta1))
        d2p = dc - math.copysign(math.pi/2, math.sin(theta2))
        v1 = p1.speed * math.cos(theta1)
        v1_perp = p1.speed * math.cos(d1p-d1)
        v2 = p2.speed * math.cos(theta2)
        v2_perp = p2.speed * math.cos(d2p-d2)
        m1 = p1.mass
        m2 = p2.mass
        v1_post = (v1*(m1-m2)+2*m2*v2) / (m1+m2)
        v2_post = (v2*(m2-m1)+2*m1*v1) / (m1+m2)
        p1.speed = math.sqrt(v1_post**2 + v1_perp**2)
        p2.speed = math.sqrt(v2_post**2 + v2_perp**2)
        p1.direction = dc + math.acos(v1_post/p1.speed)*math.sin(d1p-dc)
        p2.direction = dc + math.acos(v2_post/p2.speed)*math.sin(d2p-dc)
        p1.polar_to_cartesian()
        p2.polar_to_cartesian()

    # Updates particle position to position after time t. Ignores all collisions.
    def time_pass(self, t): 
        self.px += self.sx*t
        self.py += self.sy*t


def generate_particle_group(amount=1, 
                            radius=1,
                            speed=1, 
                            mass=1,
                            color=(0.2, 1, 0.2),
                            sx=0,
                            sy=0):
    """Generates list of particles based on input. May generate particles
    on top of existing ones. Velocity direction is random for each particle."""
    cols, rows = calc_cluster_shape(amount)
    spacing = 2 * radius + 1
    # Shift starting location so cluster center matches given coordinates.
    sx -= (cols-1) * spacing / 2
    sy -= (rows-1) * spacing / 2
    particle_group = []
    for a in range(0, cols):
        for b in range(0, rows):
            px = a*spacing+sx
            py = b*spacing+sy
            particle_group.append(
                Particle(radius, px, py, speed,
                         random.randint(0, 359) / 180 * math.pi,
                         mass,color
                         ))
            if amount == 1:
                return particle_group
            else:
                amount -= 1
    return particle_group

def calc_cluster_shape(amount):
    """Determines number of rows and columns in a 'square' cluster"""
    cols = amount**0.5
    rows = int(cols)
    if cols == rows:
        cols = rows
    elif amount <= rows * (rows+1):
        cols = rows + 1
    else:
        rows = rows + 1
        cols = rows
    return cols, rows


def clean_particles(particle_list, box_width, box_height):
    """Removes particles that don't fit in the bounding box."""
    cleaned_list = []
    for p in particle_list:
        if (p.px+p.radius >= box_width
                or p.px-p.radius <= 0
                or p.py+p.radius >= box_height
                or p.py-p.radius <= 0):
            continue
        cleaned_list.append(p)
    return cleaned_list


def generate_event_qeueu(particle_list, particle_box, time_current):
    """
    Takes particle list and generates a time ordered list of collision events.
    Output form is a list of list objects. List objects are in form:
    [event time after simulation begining, first object in event, second object in event (particle or wall)]
    """
    event_queue = []
    for p in particle_list:
        earliest_particle = None
        earliest_particle = particle_particle_earliest(p,
                                                       particle_list,
                                                       time_current,
                                                       "queue",
                                                       )
        if earliest_particle != None:
            addevent(event_queue, earliest_particle)
        earliest_wall = particle_wall_earliest(p, particle_box, time_current)
        addevent(event_queue, earliest_wall)
    return event_queue


def gen_wall(x, y, d):
    """Purpose unclear :/"""
    w = [x, y, d]
    w.append(math.tan(w[2]))
    w.append(w[1] - w[0]*w[3])
    w.append((math.sqrt(w[3]**2 + 1)))
    return w


# Calculates time to a particle-wall collision. Old version.
def particle_wall_ct2(p, w):
    # Wall direction
    w_d = math.tan(w[2])
    # Distance to wall
    w_l = (w_d*p.px - p.py + (w[1] - w[0]*w_d)) / (math.sqrt(w_d**2 + 1))
    speed = (p.speed * math.cos(p.direction - (w[2] + math.copysign(math.pi/2, w_l))))
    tc = abs(abs(w_l) - p.radius) / speed # Time to collision
    return tc

# Updated version.
def particle_wall_ct(p, w):
    # Wall direction
    w_d = w[3]
    # Distance to wall
    w_l = (w_d*p.px - p.py + w[4]) / w[5]
    speed = (p.speed * math.cos(p.direction - (w[2] + math.copysign(math.pi/2, w_l))))
    tc = abs(abs(w_l) - p.radius) / speed # Time to collision
    return tc

# Old version.
def particle_particle_ct2(p1, p2):
    """
    Calculates the time of 2 particle collision.
    Solves quadratic equation in the form t**2*a + t*b + c = 0 and chooses physicaly relevant root.
    """
    a = (p1.sx - p2.sx)**2 + (p1.sy - p2.sy)**2
    b = 2 * ((p1.px - p2.px) * (p1.sx - p2.sx) + (p1.py - p2.py) * (p1.sy - p2.sy))
    c = (p1.px - p2.px)**2 + (p1.py - p2.py)**2 - (p1.radius + p2.radius)**2
    D = b**2 - 4 * a * c
    if D < 0 or a == 0:
        return 999999999
    else:
        #t1 = (b * (-1) + math.sqrt(D)) / (2 * a)
        t2 = (b * (-1) - math.sqrt(D)) / (2 * a)
        return t2

# Updated version.
def particle_particle_ct(p1, p2):
    """
    Calculates the time of 2 particle collision.
    Solves quadratic equation in the form t**2*a + t*b + c = 0 and chooses physicaly relevant root.
    """
    sx = p1.sx - p2.sx
    sy = p1.sy - p2.sy
    dx = p1.px - p2.px
    dy = p1.py - p2.py
    a = sx**2 + sy**2
    b = 2 * ((dx) * sx + (dy) * sy)
    c = (dx)**2 + (dy)**2 - (p1.radius + p2.radius)**2
    D = b**2 - 4 * a * c
    if D < 0 or a == 0:
        return 999999999
    else:
        # t1 = (b * (-1) + math.sqrt(D)) / (2 * a)
        t2 = (b * (-1) - math.sqrt(D)) / (2 * a)
        return t2


def particle_particle_earliest(p1, particle_list, time_current, mode="single"):
    """
    Calculates earliest particle-particle event for a given particle.
    Default mode (single) is when you want to know earliest event for any given particle.
    For use in first time event queue generation pass any other string in mode argument.
    """
    earliest_event = []
    """if mode == "single":
        start = 0
    else:
        start = particle_list.index(p1)+1
    for i in range(start,
                   len(particle_list)):
        p2 = particle_list[i]
        if p1 == p2:
            continue
        ct = particle_particle_ct(p1, p2) + time_current
        if ct > time_current:
            if earliest_event == [] or ct < earliest_event[0]:
                earliest_event = [ct, p1, p2]"""
    for p2 in particle_list:
        if p1 == p2:
            continue
        ct = particle_particle_ct(p1, p2) + time_current
        if ct > time_current:
            if earliest_event == [] or ct < earliest_event[0]:
                earliest_event = [ct, p1, p2]
    return earliest_event


def particle_wall_earliest(p1, particle_box, time_current):
    """
    Calculates earliest particle-wall event for a given particle.
    """
    earliest_event = []
    for wall in particle_box:
        time_collision = particle_wall_ct(p1, wall) + time_current
        if time_collision > time_current:
            if earliest_event == [] or earliest_event[0] > time_collision:
                earliest_event = [time_collision, p1, wall]
    return earliest_event


def addevent(event_queue, event):
    if event == []:
        return
    for e in event_queue:
        if e[0] > event[0]:
            event_queue.insert(event_queue.index(e), event)
            return
    event_queue.append(event)


def event_handling(event_queue, particle_list, particle_box, time_elapsed, time_current):
    #Checking if next event happens in current frame
    while event_queue[0][0] < time_elapsed:
        current_event = event_queue.pop(0)
        time_frame = current_event[0] - time_current
        time_current = current_event[0]
        p1 = current_event[1]
        p2 = current_event[2]
        # Move simulation time to event moment.
        for p in particle_list:
            p.time_pass(time_frame)
        # Check collision type.
        if isinstance(p2, list): # Particle wall collision.
            p1.collide_wall(p2[2])
            """
            For slower simulation without bugs replace following if fork with commented code:
            event_queue = generate_event_qeueu(particle_list, particle_box, time_current)
            """
            recalc = []
            recalc.append(p1)
            for_removal = []
            for e in event_queue:
                if e[1] == p1:
                    if isinstance(e[2], Particle) and not (e[2] in recalc):
                        recalc.append(e[2])
                    for_removal.append(e)
                elif e[2] == p1:
                    if isinstance(e[1], Particle) and not (e[1] in recalc):
                        recalc.append(e[1])
                    for_removal.append(e)
            for e in for_removal:
                event_queue.remove(e)
            for p in recalc:
                earliest_particle = particle_particle_earliest(p, particle_list, time_current)
                earliest_wall = particle_wall_earliest(p, particle_box, time_current)
                if not (earliest_particle in event_queue):
                    addevent(event_queue, earliest_particle)
                if not (earliest_wall in event_queue):
                    addevent(event_queue, earliest_wall)
        else: #Particle particle collision.
            p1.collide_particles(p2)
            """
            For slower simulation without bugs replace following if fork with commented code:
            event_queue = generate_event_qeueu(particle_list, particle_box, time_current)
            """
            recalc = []
            recalc.append(p1)
            recalc.append(p2)
            for_removal = []
            for e in event_queue:
                a = (e[1] == p1 or e[1] == p2)
                b = (e[2] == p1 or e[2] == p2)
                if a:
                    if isinstance(e[2], Particle) and not (e[2] in recalc):
                        recalc.append(e[2])
                    for_removal.append(e)
                elif b:
                    if isinstance(e[1], Particle) and not (e[1] in recalc):
                        recalc.append(e[1])
                    for_removal.append(e)
            for e in for_removal:
                event_queue.remove(e)
            for p in recalc:
                earliest_particle = particle_particle_earliest(p, particle_list, time_current)
                earliest_wall = particle_wall_earliest(p, particle_box, time_current)
                if not (earliest_particle in event_queue):
                    addevent(event_queue, earliest_particle)
                if not (earliest_wall in event_queue):
                    addevent(event_queue, earliest_wall)
    # Pass remaining time in current frame.
    time_frame = time_elapsed - time_current
    for p in particle_list:
        p.time_pass(time_frame)
    time_current = time_elapsed
    return time_current, event_queue


#setting up 2D viewport in OpenGL
def refresh2d(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0.0, width, 0.0, height, 0.0, 1.0)
    glMatrixMode (GL_MODELVIEW)
    glLoadIdentity()


#self made circle drawing function; needs optimisation
def draw_circle(x, y, radius, vertices = 32, speed = 0):
    glBegin(GL_POLYGON)                                 #start drawing
    interval = 2 * math.pi / vertices
    if speed:
        glColor3f(0.1*(speed%9),1.0,1.0/(speed%9))
    for i in range(0, vertices):
        glVertex2f(x + radius * math.sin(interval * i), y + + radius * math.cos(interval * i))
    glEnd()                                             #end drawing


#empty draw function
def draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  #clear the screen
    glLoadIdentity()                                    #reset position
    glutSwapBuffers()                                   #important for double buffering


#draw function for main (second) window
def draw2():
    global time_elapsed, time_current
    global particle_list, particle_box, event_queue
    global fspeed, good
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  #clear the screen
    glLoadIdentity()                                    #reset position
    refresh2d(width, height)                            #2D viewport
    glColor3f(0.7, 1.0, 0.2)
    #simulation time passes
    if time_elapsed == 0:                               #Begin tracking time.
        time_elapsed = time.clock()
    else:
        time_current = time_elapsed                        #Time passes.
        time_elapsed = time.clock()
    event_handling(event_queue, particle_list, particle_box, time_elapsed, time_current)
    #main drawing
    for p in particle_list:
        draw_circle(p.px, p.py, p.radius)
    glutSwapBuffers()                                   #important for double buffering


def test_particleparticlecollisiontimecalculation(p1=None, p2=None):
    if p1 is None:
        p1 = Particle(2, 1, 1, 1, math.pi)
    if p2 is None:
        p2 = Particle(2, 6, 3, 1, math.pi)
    #radius, position_x, position_y, speed, direction
    print(particle_particle_ct(p1, p2))

def event_q_output(q):
    o = []
    for i in q:
        t = str(i[0]) + ', '
        t += ('P'+str(i[1].id)+', ')
        if isinstance(i[2], Particle):
            t += str('P'+str(i[2].id))
        else:
            t += str(i[2])
        o.append(t)
    result = ''
    for i in o:
        result += str(i) + '\n'
    return result


if __name__ == "__main__":
    #draw window settings
    window = 0
    window2 = 1
    height, width = 400, 500
    #particle area settings
    particle_box = [[0, 0, 0], [width, 0, math.pi/2], [0, 0, math.pi/2], [0, height, 0]] #[x, y, direction]
    #global time tracking
    time_elapsed = 0
    time_current = 0
    #particle data
    particle_list = []
    glutInit()
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
    glutInitWindowSize(width, height)           #set window size
    glutInitWindowPosition(100, 100)            #set window position
    window = glutCreateWindow(b"bandymas")      #create a window with a title
    glutDisplayFunc(draw)                       #set draw function callback
    glutIdleFunc(draw)                          #draw all the time
    window2 = glutCreateWindow(b"cookies")      #create a second window because first fails to be displayed
    glutDisplayFunc(draw2)                      #set draw function callback
    glutIdleFunc(draw2)                         #draw all the time
    #Generates random particles and first instance of event queue
    event_queue = []

    particle_list = generate_particle_group(width, height, amount=100, radius=7, speed=60)
    event_queue = generate_event_qeueu(particle_list, particle_box, time_current)

    #start everything
    glutMainLoop()                              
    glutDestroyWindow(window)                   #remove first created window, because it fails to be displayed in my system
    """
    p2 = particle(2, 1, 1, 1, 0)
    p1 = particle(2, 20, 1, 20, math.pi/2)
    test_particleparticlecollisiontimecalculation(p1, p2)
    FormulaTest(p1, p2)
    #(self, radius, position_x, position_y, speed, direction, mass = 1)
    """