===========================
Project "Atomic Simulation"
===========================

Project Goal
------------
	To create a visual tool for learning about atomic world and it's interactions.

Project Tasks
-------------
	1) Create a simulation of noble gas in a box (hard discs in a 2D box simulation).
	2) Difusion and osmosis simulation.
	3) Create a simulation of (O, N, F, Cl) atoms in a box (hard discs joining into pairs).
	4) Phase transitions of atomic substances.
	5) ...

Project state
-------------
	Working simulation of hard discs in a 2D box.

Next steps (in no particular order)
-----------------------------------
	Consulting on code quality.
	Simulation starting conditions in a file.
	Upgrade simulation algorithm by adding Cell Based method.
	Creating semipermiable walls.

Project status
--------------
	ACTIVE