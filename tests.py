import time
import math
import random
import unittest

from parameterized import parameterized

import sim_math

class TestParticle(unittest.TestCase):

    @parameterized.expand([
        (1, 1, 1, 1, 0, 1, (1, 1, 1, 1))])
    def test_init(self, rds, px, py, spd, direction, m, c):
        p = sim_math.Particle(rds, px, py, spd, direction, m, c)
        self.assertEqual(p.radius, rds)
        self.assertEqual(p.px, px)
        self.assertEqual(p.py, py)
        self.assertEqual(p.speed, spd)
        self.assertEqual(p.direction, direction)
        self.assertEqual(p.mass, m)
        self.assertEqual(p.color, c)
        self.assertNotEqual(p.id, 0)

    @parameterized.expand([
        (math.pi, math.pi),
        (math.pi*4, 0),
        (math.pi*2.5, math.pi*0.5),
        (math.pi*-1, math.pi*1),
        (math.pi*-21.25, math.pi*0.75),
        ])
    def test_direction_setter(self, direction, expected):
        rds, px, py, spd = 1, 1, 1, 1
        p = sim_math.Particle(rds, px, py, spd, direction)
        self.assertAlmostEqual(p.direction, expected)

    @parameterized.expand([
        (1, 0, 1, 0),
        (10, math.pi/2, 0.0, 10.0),
        (5, math.pi/3, 2.5, 4.330127018),
        (43, 2, -17.8943139715, 39.0997893535)
        ])
    def test_polar_to_cartesian(self, spd, direction, expsx, expsy):
        rds, px, py = 1, 1, 1
        p = sim_math.Particle(rds, px, py, spd, direction)
        self.assertAlmostEqual(p.sx, expsx)
        self.assertAlmostEqual(p.sy, expsy)

    @parameterized.expand([
        (10, 10, 14.14213562373, math.pi/4),
        (0, 15, 15, math.pi/2),
        (2, 0, 2, 0),
        (-4, 0, 4, math.pi),
        (0, -3, 3, math.pi*1.5),
        ])
    def test_cartesian_to_polar(self, sx, sy, expspeed, expdirection):
        rds, px, py, spd, direction = 1, 1, 1, 1, 1
        p = sim_math.Particle(rds, px, py, spd, direction)
        p.sx, p.sy = sx, sy
        p.cartesian_to_polar()
        self.assertAlmostEqual(p.speed, expspeed)
        self.assertAlmostEqual(p.direction, expdirection)

    @parameterized.expand([
        (10, 0, 15, math.pi, 5, math.pi),
        (18, math.pi/2, 18, math.pi*1.5, 0, 0)
        ])
    def test_change_polar_velocity(self, spd, initdirection,
                                   speed, direction,
                                   expspeed, expdirection,
                                   ):
        rds, px, py = 1, 1, 1
        p = sim_math.Particle(rds, px, py, spd, initdirection)
        p.change_polar_velocity(speed, direction)
        self.assertAlmostEqual(p.speed, expspeed)
        self.assertAlmostEqual(p.direction, expdirection)

    @parameterized.expand([
        (0, math.pi/2, math.pi),
        (0, math.pi/4, math.pi/2),
        (math.pi/4, math.pi/2, math.pi*0.75),
        (math.pi/4, 0, math.pi*1.75),
        (math.pi/8, math.pi*0.875, math.pi*1.625)
        ])
    def test_collide_wall(self, direction, walldir, expdirection):
        rds, px, py, spd = 1, 1, 1, 1
        p = sim_math.Particle(rds, px, py, spd, direction)
        p.collide_wall(walldir)
        self.assertAlmostEqual(p.speed, spd)
        self.assertAlmostEqual(p.direction, expdirection)

    @parameterized.expand([
        ((10, 0, 10), (10, math.pi, 10), (10, math.pi, 10, 0)),
        ((10, 0, 1), (10, math.pi, 10), (290/11, math.pi, 70/11, math.pi)),
        ])
    def test_collide_particles(self, obj1, obj2, expected):
        rds, px, py = 1, 1, 1
        spd1, direction1, mass1 = obj1
        spd2, direction2, mass2 = obj2
        p1 = sim_math.Particle(rds, px, py, spd1, direction1, mass1)
        p2 = sim_math.Particle(rds, px, py, spd1, direction2, mass2)
        p1.collide_particles(p2)
        values_to_assert = [p1.speed, p1.direction, p2.speed, p2.direction]
        for obtained, exp in zip(values_to_assert, expected):
            self.assertAlmostEqual(obtained, exp)

    @parameterized.expand([
        (20, 40, 10, 0, 10, (120, 40)),
        (0, 0, 0, 0, 0, (0, 0)),
        (-20, -30, -50, math.pi*0.75, 0.2, (-12.9289321881, -37.071067811865)),
        ])
    def test_time_pass(self, px, py, spd, direction, duration, expected):
        rds = 1
        p = sim_math.Particle(rds, px, py, spd, direction)
        p.time_pass(duration)
        self.assertAlmostEqual(p.px, expected[0])
        self.assertAlmostEqual(p.py, expected[1])


class TestSimMathFunc(unittest.TestCase):
    @parameterized.expand([
        (1, (1, 1)),
        (2, (2, 1)),
        (3, (2, 2)),
        (5, (3, 2)),
        (16, (4, 4)),
        (30, (6, 5)),
        (35, (6, 6)),
        ])
    def test_calc_cluster_shape(self, amount, expected):
        self.assertEqual((sim_math.calc_cluster_shape(amount)), expected)

    @parameterized.expand([
        (10, 5, 50, 50),
        (0, 0, 0, 0),
        (1, 0, 0, 0),        
        ])
    def test_generate_particle_group(self, amount, radius, sx, sy):
        p_group = sim_math.generate_particle_group(
                        amount=amount,
                        radius=radius,
                        sx=sx,
                        sy=sy
                        )
        self.assertEqual(len(p_group), amount)
        for p in p_group:
            self.assertEqual(p.radius, radius)
            self.assertEqual(p.mass, 1)

    @parameterized.expand([
        (1, 1, -2, 10, 10, 10, 0),
        (10, 1, 11, 5, 10, 10, 3),
        (1, 20, 5, 5, 20, 20, 0),
        (9, 2, 3, 3, 6, 6, 1),
        (9, 2, 3, 3, 15, 15, 4),
        (25, 2, 7.5, 7.5, 15, 15, 9),        
        ])
    def test_clean_particles(self, amount, radius, sx, sy,
                             width, height, expected
                             ):
        p_group = sim_math.generate_particle_group(
                        amount=amount,
                        radius=radius,
                        sx=sx, sy=sy,
                        )
        p_group = sim_math.clean_particles(p_group, width, height)
        self.assertEqual(len(p_group), expected)

    def test_particle_particle_earliest(self):
        pass
        # sim_math.particle_particle_earliest(p1, particle_list, time_current)

    @parameterized.expand([
        (0, 0, 1, 1, 0,
         3, 0, 1, 0, 0,
         1),
        (-3, -3, 10, 10, math.pi*0.25,
         24, 15, 1, 15, math.pi*1.25,
         0.913905037237),
        (54, 646, 2, 13, 3,
         -476, -32, 3, 32, 4,
         999999999)
        ])
    def test_particle_particle_ct(self,
                                  x1, y1, r1, s1, d1,
                                  x2, y2, r2, s2, d2,
                                  expected,
                                  ):
        p1 = sim_math.Particle(r1, x1, y1, s1, d1)
        p2 = sim_math.Particle(r2, x2, y2, s2, d2)
        self.assertAlmostEqual(sim_math.particle_particle_ct(p1, p2),
                               expected)


if __name__ == "__main__":
    unittest.main()
